﻿#include <iostream>
#define IS_INCLUDE(nIncl, nMax) ((((nIncl) < (nMax)) && ((nIncl) >= 0)) ? true : false)
#define GET_ELEM(Arr) (**Arr)
#define GET_COUNT(Arr, Type) (sizeof(Arr)/sizeof(Type))

static const int sc_nSize = 3;

int main()
{
	int** nDynArr = new int* [sc_nSize];

	for (int i = 0; i < sc_nSize; i++)
		nDynArr[i] = new int[sc_nSize];

	int nArr[sc_nSize][sc_nSize];

    std::cout << GET_COUNT(nArr, int); 
}